BINDIR=$(DESTDIR)/usr/bin
ETCDIR=$(DESTDIR)/etc
MODDIR=$(DESTDIR)/usr/share/perl5/
PACKAGE=ikiwiki-hosting
VERSION=$(shell perl -e '$$_=<>;print m/\((.*?)\)/'<CHANGELOG)

CFLAGS=-O2 -Wall -g
BINS=iki-git-shell iki-git-hook-update ikisite-wrapper
SCRIPTS=ikisite ikidns ikiwiki-hosting-web-daily ikiwiki-hosting-web-backup \
	ikisite-delete-unfinished-site iki-ssh-unsafe
MANS=ikisite ikidns ikisite-wrapper ikiwiki-hosting-web-daily \
     	ikiwiki-hosting-web-backup ikisite-delete-unfinished-site \
	iki-git-hook-update iki-git-shell iki-ssh-unsafe

IKIWIKI=LC_ALL=C TZ=UTC ikiwiki --wikiname "ikiwiki hosting internals" \
		--no-usedirs --underlaydir=/dev/null \
		--plugin goodstuff \
		--disable-plugin smiley --disable-plugin shortcut \
		--disable-plugin relativedate --disable-plugin toggle \
		--disable-plugin openid \
		--set deterministic=1 \
		doc html

include ./ikiwiki-hosting.conf

all: build test

build: $(BINS)
	$(IKIWIKI)
	
	set -e; for man in $(MANS); do \
		./mdwn2man $$man 1 doc/$$man.mdwn > $$man.1; \
	done

test:
	perl "-MExtUtils::Command::MM" "-e" "test_harness(0, '.')" t/*.t

clean:
	$(IKIWIKI) --clean
	rm -f $(BINS)
	set -e; for man in $(MANS); do \
		rm -f $$man.1; \
	done

install:
	install -d $(BINDIR)
	install -m 0755 $(SCRIPTS) $(BINS) $(BINDIR)

	install -d $(ETCDIR)/ikiwiki-hosting/config
	install -m 0644 ikiwiki-hosting.conf $(ETCDIR)/ikiwiki-hosting
	install -d $(ETCDIR)/ikiwiki-hosting/autosetup
	cp autosetup/* $(ETCDIR)/ikiwiki-hosting/autosetup
	install -d $(ETCDIR)/ikiwiki-hosting/templates
	cp templates/* $(ETCDIR)/ikiwiki-hosting/templates

	install -d $(MODDIR)/IkiWiki/Plugin $(MODDIR)/IkiWiki/Setup
	install -m 0644 IkiWiki/*.pm $(MODDIR)/IkiWiki/
	install -m 0644 IkiWiki/Plugin/*.pm $(MODDIR)/IkiWiki/Plugin/
	install -m 0644 IkiWiki/Setup/*.pm $(MODDIR)/IkiWiki/Setup/

	mkdir -p $(DESTDIR)/$(lockdir)
	mkdir -p $(DESTDIR)/$(gitdaemondir)
	mkdir -p $(DESTDIR)/$(shell dirname $(accountinglog))

dist:
	git archive --format=tar --prefix=$(PACKAGE)-$(VERSION)/ HEAD | xz -c > $(PACKAGE)-$(VERSION).tar.xz
