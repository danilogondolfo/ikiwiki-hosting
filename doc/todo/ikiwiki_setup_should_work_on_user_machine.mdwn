A user should be able to clone their site, get ikiwiki.setup from the setup
branch, and run ikiwiki on it, w/o having ikiwiki-hosting installed.

Currently, this will fail because:

* The setup file uses a per-site home directory. How to deal with this?
  User can frob paths, of course.

* The setup file uses `add_plugins` to pull in some plugins that are
  not in stock ikiwiki. Maybe instead of listing those in the
  ikiwiki.setup, they could be automatically pulled in via a tweaked
  goodstuff?
