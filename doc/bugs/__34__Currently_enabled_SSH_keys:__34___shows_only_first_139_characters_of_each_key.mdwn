At least at http://free-thursday.pieni.net/ikiwiki.cgi the "SSH keys" page shows only the first 139 characters of each SSH key. I'm using iceweasel in 1024x768 resolution and there are not scrollbars visible.

Please contact me at timo.lindfors@iki.fi

> I have access to the same wiki, and do not see the problem Timo sees. I see 380 chars of the SSH keys, and do have a scrollbar.
> Weird. --liw

> The html used to display the keys is an unstyled `<pre>`. I'd expect
> a browser to add a scrollbar to the whole page when part of it is too
> wide. Or to put a scrollbar on the individual pre with the too-long line,
> like chromium does. 
> 
> But I can reproduce the problem described
> with iceweasel 3.5.15. Instead, if you click on the line of a key, 
> you can use left/right arrows to scroll just that line, with no visible
> UI! (What a strange UI decision the mozilla people made!) --[[Joey]] 

>> This seems to be fixed in current iceweasel. [[done]] --[[Joey]] 
