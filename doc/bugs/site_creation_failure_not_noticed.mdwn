Since site creation over the web happens in the background,
there's a problem in detecting when the site creation fails.

Instead of a useful error message, the user will be dumped
into a site that failed to be created, or branched, and is in
some unknown broken state.

The code currently just checks that the site exists, and is
not locked, and assumed that means ikiwiki set it up correctly.
There should at least be some positive sign checked that ikiwiki
finished successfully.

> [[done]]; nonce checked
