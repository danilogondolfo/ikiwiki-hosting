# NAME

ikisite - ikiwiki-hosting site helper

# SYNOPSIS

ikisite subcommand options

# DESCRIPTION

ikisite manages individual sites.

# FILES

`/etc/ikiwiki-hosting/ikiwiki-hosting.conf` is the config file read by
default.

`/etc/ikiwiki-hosting/autosetup/` holds the ikiwiki setup template files.

`/etc/ikiwiki-hosting/templates/` holds other templates (eg, for apache vhost
config files).

# EXAMPLES

Basic site management:

	ikisite create myhost.ikiwiki.net --type=blog --vcs=git --admin 'http://joey.kitenet.net/'

	ikisite backup myhost.ikiwiki.net mybackup

	ikisite delete myhost.ikiwiki.net

	ikisite sudo myhost.ikiwiki.net vim ikiwiki.setup

	ikisite ikiwikisetup myhost.ikiwiki.net

# SEE ALSO

* [[ikidns]](1)
* [[ikisite-wrapper]](1)

# AUTHOR

Joey Hess <joey@ikiwiki.info>

Warning: this page is automatically made into a man page via [mdwn2man](http://git.ikiwiki.info/?p=ikiwiki;a=blob;f=mdwn2man;hb=HEAD).  Edit with care
