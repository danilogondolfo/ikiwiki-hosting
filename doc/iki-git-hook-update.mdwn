# NAME

iki-git-hook-update - ikiwiki-hosting git update hook

# SYNOPSIS

iki-git-hook-update

# DESCRIPTION

This program is installed by ikisite as the git update hook for ikiwiki
git repositories. It allows trusted sources to commit any change,
while limiting commits from untrusted sources to prevent them making arbitrary
new branches, or making unsafe changes to the site's setup branch.

# SEE ALSO

* [[ikisite]](1)

# AUTHOR

Joey Hess <joey@ikiwiki.info>

Warning: this page is automatically made into a man page via [mdwn2man](http://git.ikiwiki.info/?p=ikiwiki;a=blob;f=mdwn2man;hb=HEAD).  Edit with care
